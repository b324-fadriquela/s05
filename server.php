<?php

session_start();

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    if (isset($_POST['submit'])) {

        $username;
        $password;

        function validateEmail($data){

            if($data === "johnsmith@gmail.com"){ //"a" || true){
                $user = explode ("@", $data); 
                return $user[0];
            }
            else{
                header('Location: ./index.php?err=invalidEmail');
                exit();
            }

        }

        function validatePass($data){

            if($data === "1234"){ //"a"){
                return $data;
            }
            else{
               
                header('Location: ./index.php?err=invalidPass');
                exit();
            }

        }

        $username = validateEmail($_POST['name']);     
        $password = validatePass($_POST['password']);     


        if (!empty($username) && !empty($password)) {  
            $_SESSION['email'] = $username;   
        }
        else{
            header('Location: ./index.php?err=invalidCredentials');
        }

        if(isset( $_SESSION['email'])){
            print_r($_SESSION);
            //header('Location: ./index.php');
            header('Location: ./index.php?msg=loginSuccess');
        }


    }


    if (isset($_POST['logout'])) {

       session_unset();
       session_destroy();
       header('Location: ./index.php');
       exit();

    }

  

}


?>

